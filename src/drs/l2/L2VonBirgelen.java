package drs.l2;

import drs.l2.gui.MulticoreScheduleFrame;
import drs.l2.scheduler.Task;

/**
 *
 * @author Alexander von Birgelen
 */
public class L2VonBirgelen {
    
    //This is the amount of Task sets. Increase this number when adding more
    //test cases. The test cases are implemented in the switch-case statement 
    //below.
    private static final int TASK_SET_COUNT = 4;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //The program performs 10 Test cases:
        for(int cpu=0;cpu<3;cpu++) {
        //int cpu=1;
            for(int set=0; set<TASK_SET_COUNT;set++) {
            //int set=3;

                int p;
                
                switch(cpu) {
                    case 0: p=2; break;
                    case 1: p=5; break;
                    case 2: p=8; break;
                    default: p=1; break;
                }
                
                //The Multicore system with p identical processors
                MulticoreSystem s = new MulticoreSystem(p);

                //Select the Test case:
                //The first argument of the Constructor of Task is the Period,
                //the second parameter is the computation time.
                switch(set) {
                    case 0:
                        //5 Tasks
                        s.addTask(new Task(5,1));
                        s.addTask(new Task(6,2));
                        s.addTask(new Task(10,2));
                        s.addTask(new Task(20,2));
                        s.addTask(new Task(10,1));
                        break;
                    case 1:
                        //8 identical Tasks
                        s.addTask(new Task(8,1));
                        s.addTask(new Task(8,1));
                        s.addTask(new Task(8,1));
                        s.addTask(new Task(8,1));
                        s.addTask(new Task(8,1));
                        s.addTask(new Task(8,1));
                        s.addTask(new Task(8,1));
                        s.addTask(new Task(8,1));
                        break;
                    case 2:
                        //4 Tasks.
                        s.addTask(new Task(5,1));
                        s.addTask(new Task(9,3));
                        s.addTask(new Task(30,1));
                        s.addTask(new Task(30,11));
                        break;
                    case 3:
                        //8 Tasks (from lecture slide)
                        s.addTask(new Task(10,2));
                        s.addTask(new Task(20,5));
                        s.addTask(new Task(40,8));
                        s.addTask(new Task(80,16));
                        s.addTask(new Task(10,3));
                        s.addTask(new Task(40,16));
                        s.addTask(new Task(20,4));
                        s.addTask(new Task(10,6));
                        break;
                    default:
                        System.out.println("Wrong Task set selection.");
                        return;
                }

                //Calculate the schedule:
                s.createMultiCoreSchedule();

                //Print the calculated schedule:
                s.printSchedule();

                //To visualize the calcuated schedule a GUI is needed:
                MulticoreScheduleFrame gui = new MulticoreScheduleFrame(s);
                //Show gui:
                gui.setVisible(true);
                //The gui can save the Plot as a png file by calling this method:
                gui.save("P-" + p + "-Set-"+set);

            }
        }
    }
    
}
