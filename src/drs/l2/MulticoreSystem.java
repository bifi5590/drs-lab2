
package drs.l2;

import drs.l2.scheduler.Scheduler;
import drs.l2.scheduler.Task;
import drs.l2.scheduler.algorithms.RateMonotonic;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

/**
 *
 * @author Alexander von Birgelen
 */
public class MulticoreSystem {
    
    //The number of processors
    private final int p;
    
    //List for Task sets (one for each processor):
    private LinkedList<Scheduler> taskSets;
    //List of tasks (before schedule is created):
    private LinkedList<Task> taskList;
    
    /**
     * Create a new Multi core system with the specified amount of processors.
     * @param processors Number of processors.
     */
    public MulticoreSystem(int processors) {
        this.p = processors;
        this.taskSets = new LinkedList<>();
        
        for(int i=0;i<p;i++) {
            //At the moment RateMonotonic is the only scheduler implemented. This may be changed in the future.
            taskSets.add(new RateMonotonic());
        }
        
        this.taskList = new LinkedList<>();
    }
    
    /**
     * Add a new Task to the system.
     * @param t the task
     * @return true on success.
     */
    public boolean addTask(Task t) {
        return taskList.add(t);
    }
    
    /**
     * Create the schedule for the processors.
     * 
     *Simple approach:
     *Task distribution need optimization algorithm because its an NP problem.
     *This program adds the task with the lowest utilization to the processor with the lowest utilization.
     *It then generates the schedule for the chosen processor. When the resulting schedule is not feasible
     *the program continues with the other processors.
     * 
     */
    public void createMultiCoreSchedule() {
        
        //Work as long as tasks are left:
        while(taskList.size() > 0) {
            
            //Sort task list (lowest utilization first):
            this.sortTaskList();
            //Sort task sets (lowest total utilization first):
            this.sortTaskSets();
            
            //Get task with lowest utilization:
            Task t = taskList.getFirst();
            
            //Try to add task to a task set and generate a feasible schedule:
            for(int i=0;i<p;i++) {
                
                //Add Task to task set:
                taskSets.get(i).addTask(t);
                
                //Generate the schedule:
                taskSets.get(i).createSchedule();
                
                //When task set is feasible then remove task from task list.
                //If the schedule is not feasible remove the task from the task set (because it gets added before)
                if ( taskSets.get(i).isScheduleFeasible() ) {
                    taskList.removeFirst();
                    t = null; //t equal null indicates that the task was added to a task set.
                    break;
                }else{
                    taskSets.get(i).dropSpecificTask(t);
                }
                
            }
            
            //Abort when the task could not be added to any processor:
            if(t != null) {
                System.out.println("Could not create a feasible schedule!");
                return;
            }
            
        }
        
        System.out.println("Schedule ready!");
        
    }
    
    /**
     * Sort the task sets ascending to their total utilization.
     */
    private void sortTaskSets() {
        Collections.sort(this.taskSets, 
        new Comparator<Scheduler>(){

            @Override
            public int compare(Scheduler o1, Scheduler o2) {
                double diff = o1.getUtilization() - o2.getUtilization();
                
                if (diff > 0) {
                    return 1;
                }else if( diff < 0) {
                    return -1;
                }else{
                    return 0;
                }
                
            }
            
        }
        );
    }
    
    /**
     * Sort the task list ascending to the utilization.
     */
    private void sortTaskList() {
        Collections.sort(taskList, 
        new Comparator<Task>(){

            @Override
            public int compare(Task o1, Task o2) {
                
                double o1Util = (double)o1.getC() / (double)o1.getT();
                double o2Util = (double)o2.getC() / (double)o2.getT();
                
                if (o1Util > o2Util) {
                    return 1;
                }else if (o1Util < o2Util) {
                    return -1;
                }else{
                    return 0;
                }

            }
            
        }
        );
    }
    
    /**
     * Print the scheduling tables on the command line.
     */
    public void printSchedule() {
        
        for(int i=0; i<p; i++) {
            System.out.println("Processor #"+i);
            taskSets.get(i).printSchedule(true);
        }
        
    }
    
    /**
     * Get the processor count
     * @return the number of processors
     */
    public int getProcessorCount() {
        return this.p;
    }
    
    /**
     * Get the task sets.
     * @return 
     */
    public LinkedList<Scheduler> getTaskSets() {
        return taskSets;
    }
    
}
