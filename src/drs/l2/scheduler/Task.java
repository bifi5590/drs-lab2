package drs.l2.scheduler;

/**
 *
 * @author Alexander von Birgelen
 */
public class Task {

    //The period of the Task:
    private final int T;
    //The computation time of the Task:
    private final int C;

    /**
     * Create a new Task with Period T and Computation time C.
     * @param T
     * @param C 
     */
    public Task(int T, int C) {
            this.T = T;
            this.C = C;
    }

    /**
     * Get the Period of the task.
     * @return 
     */
    public int getT() {
            return T;
    }

    /**
     * Get the computation time of the task.
     * @return 
     */
    public int getC() {
            return C;
    }

    /**
     * Return this task as a short String.
     * @return 
     */
    @Override
    public String toString() {
        return "#[T="+T+";C="+C+"]";
    }

}
