package drs.l2.scheduler;

import java.util.LinkedList;
import java.util.Locale;

/**
 *
 * @author Alexander von Birgelen
 */
public abstract class Scheduler {
    
    //The task set:
    protected LinkedList<Task> tasks;
    //This is a one row array which holds the schedule (except missed deadlines):
    protected int[] sched;
    //This is the schedule. One row is for one task. The columns are the time slots.
    protected int[][] schedule;
    //Indication if the schedule is feasible:
    protected boolean feasible;
    
    /**
     * Initialize the new Scheduler.
     */
    public Scheduler() {
        tasks = new LinkedList<>();
    }
    
    //Add a new Task to the scheduler:
    public boolean addTask(Task t) {
        return tasks.add(t);
    }
    
    //Remove a task:
    public boolean dropTask(int i) {
        Task t = tasks.remove(i);
        return t != null;
    }
    
    public boolean dropSpecificTask(Task t) {
        return tasks.remove(t);
    }
    
    //Return task set:
    public LinkedList<Task> getTasks() {
        return tasks;
    }
    
    /**
     * Checks if all elements of an array are equal.
     * Used for calculation of LCM period.
     * @param multiple array to check.
     * @return True when all elements are the same.
     */
    private boolean checkequal(int[] multiple) {
        //last checked element:
        int last = multiple[0];
        //Loop through all elements:
        for(int i : multiple) {
            //If one element is found which is not equal to the last one return
            //false.
            if(i != last) {
                return false;
            }
            last = i;
        }
        return true;
    }
    
    /**
     * Calculate the LCM Period of the task set.
     * @return LCM
     */
    public int getLCMPeriod() {
        
        if(tasks.size() == 0) {
            return 1;
        }
        
        //Lazy:
        int[] multiple = new int[tasks.size()];

        //Init array with start values:
        for(int i=0;i<tasks.size();i++) {
            multiple[i] = tasks.get(i).getT();
        }
        

        int min; //index of minimum value
        int minval; //Minimum value
        
        while(!checkequal(multiple)) {

            //find minimum value:
            min = 0; //index of minimum value
            minval = Integer.MAX_VALUE; //Minimum value
            
            for(int i=0;i<multiple.length;i++) {
                if(multiple[i] < minval) {
                    minval = multiple[i];
                    min = i;
                }
            }
            
            //Increment smallest value:
            multiple[min] += tasks.get(min).getT();
            
        }
        
        return multiple[0];
        
    }
    
    /**
     * Return the utilization of the task set
     * @return 
     */
    public double getUtilization() {
        //Set u to zero and sum up all single utilizations:
        double u = 0;
        for(Task t : tasks) {
            //Utilization of single task:
            u += (double)t.getC()/(double)t.getT();
        }
        
        //Only show 3 digits after decimal point:
        String format = String.format(Locale.ENGLISH, "%1.3f", u);
        return Double.parseDouble(format);
    }
    
    /**
     * Calculate the least upper bound for the utilization:
     * @return 
     */
    public double getUtilizationLeastUpperBound() {
        //Utilization:
        double u = (double)tasks.size()*(Math.pow(2.0, 1/(double)tasks.size())-1.0);
        
        //Only show 3 digits after decimal point:
        String format = String.format(Locale.ENGLISH, "%1.3f", u);
        return Double.parseDouble(format);
    }
    
    /**
     * Create the new schedule!
     * @return 
     */
    public abstract int[][] createSchedule();
    
    //Return indication if schedule is feasible:
    public boolean isScheduleFeasible() {
        return feasible;
    }
    
    /**
     * Sort the Tasks. The sorting can be different for different schduling schemes.
     */
    public abstract void sortTasks();
    
    /**
     * Return the schedule:
     * @return the schedule (2dimensional array)
     */
    public int[][] getSchedule() {
        return schedule;
    }
    
    /**
     * Print the schedule in the command line.
     * @param info 
     */
    public void printSchedule(boolean info) {
        
        if(this.schedule == null) {
            this.schedule = createSchedule();
        }
        
        if(info) {
            System.out.println("LCM: "+getLCMPeriod());
            System.out.println("Utilization: "+getUtilization());
            System.out.println("Utilization (Upper bound): "+getUtilizationLeastUpperBound());
            System.out.println("Schedule feasible: "+isScheduleFeasible());
        }
        
        System.out.println("Schedule:");
        
        for(int j=0;j<tasks.size();j++) {
            System.out.print(tasks.get(j));

            System.out.print("\t| ");
            for(int i=0;i<schedule[j].length;i++) {
                if(i == schedule[j].length-2) {
                    System.out.print(schedule[j][i] + " ||| ");
                }else{
                    System.out.print(schedule[j][i] + " | ");
                }
            }
            System.out.print("\n");
        }
    }
    
    /**
     * Represents the scheduler as short String.
     * @return 
     */
    @Override
    public String toString() {
        return "#Tasks: "+tasks.size()+"   LCM: "+this.getLCMPeriod()+"   U:"+this.getUtilization()+"   Feasible:"+this.isScheduleFeasible();
    }

}
