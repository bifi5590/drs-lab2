package drs.l2.scheduler.algorithms;

import drs.l2.scheduler.Scheduler;
import drs.l2.scheduler.Task;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author Alexander von Birgelen
 */
public class RateMonotonic extends Scheduler {

    /**
     * Create the schedule using Rate Monotonic.
     * @return 
     */
    @Override
    public int[][] createSchedule() {
        
        //Sort the tasks:
        this.sortTasks();
        
        feasible = true;
        
        //Get lcm period and create the arrays for the schedule:
        int lcm = this.getLCMPeriod();
        this.sched = new int[lcm+1];
        this.schedule = new int[tasks.size()][lcm+1];
        
        //Loop through all tasks:
        for(int t=0;t<tasks.size();t++) {
            
            Task tsk = tasks.get(t);
            int s;
            
            //Loop through intervals:
            for(int rt=0;rt<(lcm/tsk.getT());rt++) {
                
                int ct = tsk.getC();
                
                //Loop through time slots:
                int start = rt*tsk.getT();
                if(start < 0) {
                    start = 0;
                }
                for(s = start;s<(rt+1)*tsk.getT();s++) {
                    //Found free slot?
                    if(sched[s] == 0) {
                        //Claim slot if it is free.
                        sched[s] = t+1;
                        schedule[t][s] = 1;
                        ct--;
                    }
                    
                    //Leave loop when no more computation time is needed:
                    if(ct==0) {
                        break;
                    }
                    
                }
                
                //Check if deadline was missed:
                if(ct > 0) {
                    //Deadline missed!
                    System.out.println("Task "+tsk+" missed deadline at Slot "+s+".");
                    schedule[t][s] = -1;
                    //Indicate which task missed its deadline here:
                    sched[s] = (t+1)*(-1);
                    feasible = false;
                }

            }
            
        }
        
        return schedule;
    }

    /**
     * Sort the task ascending to their period (smallest period first).
     */
    @Override
    public void sortTasks() {
        //Sort the List accoring to Period.
        //Shortest period comes first.
        Collections.sort(tasks, 
        new Comparator<Task>(){

            @Override
            public int compare(Task o1, Task o2) {
                return o1.getT() - o2.getT();
            }
            
        }
        );
    }
    
}
