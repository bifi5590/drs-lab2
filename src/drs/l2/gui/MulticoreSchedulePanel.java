package drs.l2.gui;

import drs.l2.MulticoreSystem;
import drs.l2.scheduler.Scheduler;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author Alexander von Birgelen
 */
public class MulticoreSchedulePanel extends JPanel {
    
    //The scheduler to use:
    private MulticoreSystem sys;
    
    //Some static values for placing the drawn elements:
    private final int leftspace = 100;
    private final int infospace = 65;
    private final int textspace = 10;
    
    /**
     * Set the schedule to use for plotting:
     * @param sys the multicore system to use.
     */
    public void setSchedule(MulticoreSystem sys) {
        this.sys = sys;
        //Try to force repaint after setting schedule:
        this.invalidate();
        this.repaint();
    }
    
    /**
     * Paint the schedule.
     * It is best to look at a plot first and then look at the source. It is
     * easier to understand the placing of the different elements in the plot 
     * then.
     * @param g 
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        //Only paint when a schedule was set:
        if(sys == null) {
            return;
        }
        
        //Draw color information:
        g.setColor(Color.green);
        g.fillRect(0*(infospace), textspace, textspace, textspace);
        g.setColor(Color.black);
        g.drawString("Work", 0*(infospace)+textspace+textspace/2, 2*textspace);
        
        g.setColor(Color.red);
        g.fillRect(1*(infospace), textspace, textspace, textspace);
        g.setColor(Color.black);
        g.drawString("Miss", 1*(infospace)+textspace+textspace/2, 2*textspace);
        
        g.setColor(Color.blue);
        g.fillRect(2*(infospace), textspace, 2, textspace);
        g.setColor(Color.black);
        g.drawString("Period", 2*(infospace)+textspace/2, 2*textspace);
        
        
        for(int cpu=0;cpu<sys.getProcessorCount();cpu++) {
            
            //Get the height values to allow dynamic painting when resizing
            int height = (this.getHeight() - 3*textspace) / sys.getProcessorCount();
            int width = this.getWidth();
            
            //Paint the schedule of one processor:
            paintSchedule(g, sys.getTaskSets().get(cpu),height, width, cpu * height , "P#"+cpu+"   "+sys.getTaskSets().get(cpu));
            
        }
        
        
    }
    
    /**
     * Paint the schedule of one processor.
     * @param g The graphics of the panel
     * @param s The Scheduler to use
     * @param height The height in pixel for the processor
     * @param width //The width of the panel
     * @param yOffset //The Offset i y direction (to not overlap with other processors)
     * @param cpu //Information string to paint.
     */
    public void paintSchedule(Graphics g, Scheduler s, int height, int width, int yOffset, String cpu) {
        
        int taskcount = s.getTasks().size();
        
        //Set taskcount to one in case the processor is unused (avoid division by zero)
        if(taskcount == 0) {
            taskcount = 1;
        }
        
        //HeightPerTask is the amount of pixels for one Task in the y direction.
        int HeightPerTask = (int) ((height) / (taskcount));
        //WidthPerSlot is the amount of pixels for one time slot in x direction.
        int WidthPerSlot = (width-leftspace) / s.getLCMPeriod();

        //Paint Slot Grid:
        for(int i=0;i<=s.getLCMPeriod();i++) {
            g.setColor(Color.LIGHT_GRAY);
            g.drawLine(leftspace + i*WidthPerSlot, 3*textspace+yOffset, leftspace + i*WidthPerSlot, height + 3*textspace+yOffset);
        }
        
        //Paint every Task:
        for(int i=0; i<s.getTasks().size();i++) {
            
            //Paint Name:
            g.setColor(Color.black);
            g.drawString(s.getTasks().get(i).toString(), textspace, i*HeightPerTask+HeightPerTask/2 + 3*textspace+yOffset);

            //Paint Slots:
            for(int j=0; j<s.getLCMPeriod()+1;j++) {
                
                if(s.getSchedule()[i][j] > 0) {
                    g.setColor(Color.green);
                    g.fillRect(leftspace + j*WidthPerSlot, i*HeightPerTask + 3*textspace+yOffset, WidthPerSlot, HeightPerTask);
                }else if(s.getSchedule()[i][j] < 0) {
                    g.setColor(Color.red);
                    g.fillRect(leftspace + j*WidthPerSlot, i*HeightPerTask + 3*textspace+yOffset, WidthPerSlot, HeightPerTask);
                }
                
            }
            
            //Paint Periods:
            for(int j=0; j<=s.getLCMPeriod();j+=s.getTasks().get(i).getT()) {
                g.setColor(Color.blue);
                g.drawLine(leftspace + j*WidthPerSlot, i*HeightPerTask + 3*textspace+yOffset, leftspace + j*WidthPerSlot, i*HeightPerTask + HeightPerTask + 3*textspace+yOffset);
                g.drawLine(leftspace + j*WidthPerSlot-1, i*HeightPerTask + 3*textspace+yOffset, leftspace + j*WidthPerSlot-1, i*HeightPerTask + HeightPerTask + 3*textspace+yOffset);
            }
            
        }
        
        
         //Print information:
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(textspace, 3*textspace+yOffset, width, textspace+1);
        g.setColor(Color.black);
        g.drawString(cpu, textspace, 4*textspace+yOffset);
        
        //Draw borderline between cpus:
        g.drawLine(0, 3*textspace+yOffset-2, width, 3*textspace+yOffset-2);
        
    }
    
}
